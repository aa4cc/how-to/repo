# How to structure and set up a git(lab/hub) repo for a project with code and data

In this how-to we collect some dos and don'ts for structuring and setting up a git repository (directory) for a project that contains both some source code and some experimental data. 

A lot of our projects – both research and educational ones – are of this kind: we write some software (or at least configure/parameterize some existing one) with which we then conduct an experiment and collect experimental data; we then write another software to analyze the experimental data (perhaps system identification methods to get a model) and perhaps yet some more code (to design a controller and/or estimator); and this we typically run another experiment and validate. This we typically repeat in a few iterations. 

Although every project imposes its own specific requirements on the structure of the repository, some core structure can certainly be recommended for most if not all projects. This document as an attempt to design collectively some style guide. 

# Recommended structure of the repository

The structure proposed here is based on tips collected from various resources, one of them being the [DrWatson.jl](https://juliadynamics.github.io/DrWatson.jl/dev/) project assisant tool for Julia (in particular their proposed repository structure is described [here](https://juliadynamics.github.io/DrWatson.jl/dev/project/)), which is perhaps inspiration even if Julia is actually not used in the project.

- `admin` (possibly some nontechnical stuff related to management of the project)
- `data`
    - `experiment_id_a`  (the name should uniquely reflect the main characteristics of the experiment: date, place, technology, partner)
        - `raw`
        - `preprocessed`
    - `experiment_id_b`
        - `raw`
        - `preprocessed`
    - `simulation_id_c`  (indeed, some simulation outcomes might be worth saving)  
- `docs` (intended purely for documentation generated from docstrings in the source codes)
- `figures` (will be recycled by reports, papers, theses, presentations)
- `reports` (and papers, theses, ...)
- `research` (whatever WIP: notes, code snippets, comments, ...)
- `scripts` (whatever is used within the project in this repo: simulations, plotting, analyses, ... Perhaps worth further structuring.)
- `src` (difference wrt scripts is that from sources we generate libs that can be used outside the repo)
- `tests` (for the sources, if there are any)
- `README.md` (should be not only descriptive but preferably also attractive)

The proposed structure is certainly not compulsory. It need not suit all projects perfectly. In particular, the following may be disputable
- Should the scripts for getting the experimental data be stored separately from the data themselves? It is possible to find good reasons for doing it, but equally good reasons for not doing so.

# Data structures for the experimental data (time series)
- In Matlab: [timetable](https://www.mathworks.com/help/matlab/timetables.html) (timeseries deprecated)
- In Julia: [DataFrames.jl](https://github.com/JuliaData/DataFrames.jl), [TSx.jl](https://github.com/xKDR/TSx.jl) (perhaps better than [TimeSeries.jl](https://github.com/JuliaStats/TimeSeries.jl) as it integrates with DataFrames.jl)...
- In Python: ...

# Format of data files
Text (CSV) vs binary.

# Naming conventions for the data files.
...
